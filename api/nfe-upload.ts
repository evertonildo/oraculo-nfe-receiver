import { VercelRequest, VercelResponse } from '@vercel/node';
import fs from 'fs';

export default (_req: VercelRequest, res: VercelResponse) => {
  const date = new Date().toString();
  res.status(200).send(date);
};